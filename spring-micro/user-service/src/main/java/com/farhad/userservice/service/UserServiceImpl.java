package com.farhad.userservice.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.farhad.userservice.Repository.UserRepository;
import com.farhad.userservice.client.APIClient;
import com.farhad.userservice.dto.DepartmentDto;
import com.farhad.userservice.dto.ResponseDto;
import com.farhad.userservice.dto.UserDto;
import com.farhad.userservice.model.User;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
	/* private RestTemplate restTemplate; */
    
    private APIClient apiClient;

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public ResponseDto getUser(Long userId) {

		/*
		 * ResponseDto responseDto = new ResponseDto(); User user =
		 * userRepository.findById(userId).get(); UserDto userDto = mapToUser(user);
		 * 
		 * ResponseEntity<DepartmentDto> responseEntity = restTemplate
		 * .getForEntity("http://localhost:8060/api/departments/" +
		 * user.getDepartmentId(), DepartmentDto.class);
		 * 
		 * DepartmentDto departmentDto = responseEntity.getBody();
		 * 
		 * System.out.println(responseEntity.getStatusCode());
		 * 
		 * responseDto.setUser(userDto); responseDto.setDepartment(departmentDto);
		 * 
		 * return responseDto;
		 */
    	
    	  ResponseDto responseDto = new ResponseDto();
          User user = userRepository.findById(userId).get();
          UserDto userDto = mapToUser(user);

          DepartmentDto departmentDto = apiClient.getDepartmentById(user.getDepartmentId());
          responseDto.setUser(userDto);
          responseDto.setDepartment(departmentDto);

          return responseDto;
    }

    private UserDto mapToUser(User user){
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        return userDto;
    }
}