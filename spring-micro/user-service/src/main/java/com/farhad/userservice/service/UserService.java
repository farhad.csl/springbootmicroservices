package com.farhad.userservice.service;

import com.farhad.userservice.dto.ResponseDto;
import com.farhad.userservice.model.User;

public interface UserService {
    User saveUser(User user);

    ResponseDto getUser(Long userId);
}