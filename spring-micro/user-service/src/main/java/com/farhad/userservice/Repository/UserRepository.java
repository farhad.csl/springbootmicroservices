package com.farhad.userservice.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.farhad.userservice.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
