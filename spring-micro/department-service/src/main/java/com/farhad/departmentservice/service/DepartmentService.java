package com.farhad.departmentservice.service;

import com.farhad.departmentservice.model.Department;

public interface DepartmentService {
	Department saveDepartment(Department department);

    Department getDepartmentById(Long departmentId);
}
