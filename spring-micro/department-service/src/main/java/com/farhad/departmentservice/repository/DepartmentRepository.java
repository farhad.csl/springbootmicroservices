package com.farhad.departmentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.farhad.departmentservice.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long>{

}
